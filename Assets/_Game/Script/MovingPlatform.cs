using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private Transform aPoint, bPoint;
    [SerializeField] private float speed;
    Vector3 target;
    private void Start()
    {
        transform.position = aPoint.position;
        target = bPoint.position;
    }
    private void Update()
    {
        // Vi tri bang di chuyen tu vi tri hien tai toi muc tieu voi toc do
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if(Vector3.Distance(transform.position, aPoint.position) < 0.1f)
        {
            target = bPoint.position;
        }else if (Vector3.Distance(transform.position, bPoint.position) < 0.1f)
        {
            target = aPoint.position;
        }
    }

    //khi Player va cham vs "square" thi Player tro thanh con cua "square" => di chuyen cung nhau
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.transform.SetParent(null);
        }
    }
}
