using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class Player : Character
{
    private float horizontal;
    private float speed = 8f;
    private float jumpingPower = 8f;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Kunai kunaiPrefab;
    [SerializeField] private Transform throwPoint;
    [SerializeField] private GameObject attackArea;

    private bool isGrounded;


    private int coin = 0;

    private Vector3 savePoint;

    private void Awake()
    {
        coin = PlayerPrefs.GetInt("coin", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDead)
        {
            return;
        }
        isGrounded = IsGounded();
        MovingPlayer();
    }

    public override void OnInit()
    {
        base.OnInit();
        transform.position = savePoint;
        SavePoint();
        ChangeAnim("idle");
        DeActiveAttack();

        SavePoint();
        UIManager.Instance.SetCoin(coin);
    }

    public override void OnDespawn()
    {
        base.OnDespawn();
        OnInit();
    }

    public override void OnDeath()
    {
        base.OnDeath();
    }

    private void MovingPlayer()
    {
        //horizontal = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        if (Mathf.Abs(horizontal) > 0.1f)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, horizontal > 0 ? 0 : 180, 0));
        }

   
        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            ChangeAnim("fall");
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        if (isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }
            if (Mathf.Abs(horizontal) > 0.1f)
            {
                ChangeAnim("run");
                //Neu horizontal >0 thi ->0, nguoc lai <0 thi -> 180
              
            }
            else
            {
                ChangeAnim("idle");
            }

            if (Input.GetKey(KeyCode.F))
            {
                Attack();
            }
            if (Input.GetKey(KeyCode.C))
            {
                Throw();
            }
        }
        else if (!isGrounded && rb.velocity.y < 0)
        {
        
            ChangeAnim("fall");
            //transform.rotation = Quaternion.Euler(new Vector3(0, horizontal > 0 ? 0 : 180, 0));
        }

       
    }

    private bool IsGounded()
    {
        Debug.DrawLine(transform.position, transform.position + Vector3.down * 1.1f, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1.1f, groundLayer);
        return hit.collider != null;
    }

    public void Jump()
    {
        ChangeAnim("jump");
        rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
    }
    public void Attack()
    {
        ChangeAnim("attack");
        Invoke(nameof(ResetAttack), 0.5f);
        ActiveAttack();
        Invoke(nameof(DeActiveAttack), 0.5f);
    }
    public void Throw()
    {
        ChangeAnim("throw");
        Invoke(nameof(ResetAttack), 0.5f);
        Instantiate(kunaiPrefab, throwPoint.position , throwPoint.rotation);
       
    }

    private void ResetAttack()
    {
        ChangeAnim("idle");
    }

    public void SavePoint()
    {
        savePoint = transform.position;
    }

    private void ActiveAttack()
    {
        attackArea.SetActive(true);
    }
    private void DeActiveAttack()
    {
        attackArea.SetActive(false);
    }

    public void SetMove(float horizontal)
    {
        this.horizontal = horizontal;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin")
        {
            //Debug.Log("Coin " + collision.gameObject.name);
            if (collision.tag == "Coin")
            {
                coin++;
                PlayerPrefs.SetInt("coin", coin);
                UIManager.Instance.SetCoin(coin);
                Destroy(collision.gameObject);
            }
        }
        if (collision.tag == "DeathZone")
        {
            ChangeAnim("die");

            Invoke(nameof(OnInit), 1f);
        }
    }
}
