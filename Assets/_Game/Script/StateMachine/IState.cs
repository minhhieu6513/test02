using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState 
{
    //Bat dau vao State
    void OnEnter(Enemy enemy);

    //Update 
    void OnExecute(Enemy enemy);

    //Thoat khoi State
    void OnExit(Enemy enemy);
}
