using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected Animator anim;
    [SerializeField] protected HealthBar healthBar;
    [SerializeField] protected CombatText CombatTextPrefab;

    private float hp;
    protected string currentAnimName;

    // => co tac dung giong nhu return
    public bool IsDead => hp <= 0;
    private void Start()
    {
        OnInit();
        healthBar.OnInit(100, transform);
    }


    //mot Object chua cac thong so ma minh muon thay doi thi luon su dung ham OnInit va OnDespawn
    //OnInit Giong ham tao
    public virtual void OnInit()
    {
        hp = 100;
    }

    //OnDespawn Giong ham huy
    public virtual void OnDespawn()
    {

    }
    public virtual void OnDeath()
    {
        ChangeAnim("die");
        Invoke(nameof(OnDespawn), 2f);
    }

    protected void ChangeAnim(string animName)
    {
        if (currentAnimName != animName)
        {
            anim.ResetTrigger(animName);
            currentAnimName = animName;
            anim.SetTrigger(currentAnimName);
        }

    }

    public void OnHit(float damage)
    {
        Debug.Log("Hit");
        if (!IsDead)
        {
            hp -= damage;
            if (IsDead)
            {
                hp = 0;
                OnDeath();
            }

            healthBar.SetNewHp(hp);
            //Prefab , vi tri, goc xoay (identy mac dinh la (0,0,0))
            Instantiate(CombatTextPrefab, transform.position + Vector3.up, Quaternion.identity).OnInit(damage);
        }
    }
}
